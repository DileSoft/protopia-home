# Дом Протопии

Данный репозиторий содержит публичную документацию социального проекта "Дом Протопии".

Если у вас есть замечания, предложения к команде проекта, то вы можете добавить issues (обсуждения): https://gitlab.com/protopiahome-public/protopia-home/-/issues

Также можно добавлять issues через Telegram-бота https://t.me/protopia_bot, отправив ему команду `/wish Текст предложения`

##  Build Instructions

With Docker Compose:

```bash
# Site:
$ docker-compose run --rm foliant make site
# PDF:
$ docker-compose run --rm foliant make pdf
```

With pip and stuff (requires Python 3.6+, Pandoc, and TeXLive):

```bash
$ pip install -r requirements.txt
# Site:
$ foliant make site
# PDF:
$ foliant make pdf
```
